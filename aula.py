#!/usr/bin/python3

from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *
from PPlay.collision import *
from PPlay.sound import *
from ball import ball_movement
from render import render
import random
from move import move, move2, mmouse
from cpu import cpu_move

state = 0 #gamestate


ww = 800
wh = 400
janela = Window(ww,wh)
nomes = ["The Game",
         "Despacito",
         "dame tu cosita",
         "Сука Блять!",
         "(☭ ͜ʖ ☭)",
         "E",
         "Jojo Reference",
         "RIP Harambe",
         "Twitter do Elon Musk",
         "REEEEEEEEEEEEEEEEEEEEEEEEEEE",]
fundo = ["img/wall.jpg",
         "img/hmm.jpg",
         "img/preso.jpg",
         "img/social.jpg",
         "img/wow.jpg",
         "img/dolphin.jpg"]
mlg = ["WOW!",
       "Press F to pay respects",
       "sample text",
       "360 no scope!",
       "ZA WARUDO!",
       "git gud",
       "dancing_snoop_dog.gif",
       """What the fuck did you just fucking say about me, you little bitch? 
       I'll have you know I graduated top of my class in the Navy Seals, and
       I've been involved in numerous secret raids on Al-Quaeda, and I have
       over 300 confirmed kills. I am trained in gorilla warfare and I'm the
       top sniper in the entire US armed forces. You are nothing to me but
       just another target. I will wipe you the fuck out with precision the
       likes of which has never been seen before on this Earth, mark my
       fucking words. You think you can get away with saying that shit to
       me over the Internet? Think again, fucker. As we speak I am contacting
       my secret network of spies across the USA and your IP is being traced
       right now so you better prepare for the storm, maggot. The storm that
       wipes out the pathetic little thing you call your life. You're fucking
       dead, kid. I can be anywhere, anytime, and I can kill you in over seven
       hundred ways, and that's just with my bare hands. Not only am I extensively
       trained in unarmed combat, but I have access to the entire arsenal of the
       United States Marine Corps and I will use it to its full extent to wipe
       your miserable ass off the face of the continent, you little shit.
       If only you could have known what unholy retribution your little "clever"
       comment was about to bring down upon you, maybe you would have held
       your fucking tongue. But you couldn't, you didn't, and now you're
       paying the price, you goddamn idiot. I will shit fury all over you and
       you will drown in it. You're fucking dead, kiddo."""
       ]
mlg_boolean = 0
mlg_x = 0
mlg_y = 0
mlg_text = ""
title = random.choice(nomes)
janela.set_title(title)

meme = GameImage(random.choice(fundo))

bg = GameImage('img/flg.jpg')

bolas = Sprite('img/balls.png')
bolas.set_position(ww/2, wh/2)

key = Window.get_keyboard()
mouse = Window.get_mouse()


pad0 = Sprite('img/pddl0.png')
pad0.set_position(0, 163)
pad1 = Sprite('img/pddl1.png')
pad1.set_position(0,200)
pad2 = Sprite('img/pddl0.png')
pad2.set_position(750,163)
pad3 = Sprite('img/pddl1.png')
pad3.set_position(750,200)

speed = 100 #velocidade, aumentará a cada encontro da bola com o paddle
psd = 350 #velocidade do paddle
psu = -350 #na direção oposta


bx = False
by = False
''' Controle de direções. Ao atingir a borda da tela na horizontal
ou o paddle na vertical, bx|by mudariam seu estado,
fazendo com que a bola vá na outra direção na horizontal|vertical '''

def cpu():
    if bolas.y != pad2.y:
        pad2.m

'''
SOUNDTRACK
'''
jojo = Sound('bgm/jojo.ogg')
despacito = Sound('bgm/despacito_2.ogg')
naruto = Sound('bgm/naruto.ogg')
airhorn_spam = Sound('bgm/airhorn_spam.ogg')
airhorn_echo = Sound('bgm/airhorn_echo.ogg')
airhorn_distant = Sound('bgm/airhorn_distant.ogg')
airhorn_tripletap = Sound('bgm/airhorn_tripletap.ogg')
airhorn_default = Sound('bgm/airhorn_default.ogg')

jojo.set_repeat(True)
naruto.set_repeat(True)
despacito.set_repeat(True)
jojo.set_volume(100)
despacito.set_volume(50)
naruto.set_volume(100)
airhorn_spam.set_volume(75)
airhorn_echo.set_volume(75)
airhorn_distant.set_volume(100)
airhorn_tripletap.set_volume(75)
airhorn_default.set_volume(75)

horn_list = [airhorn_spam, airhorn_echo, airhorn_distant, airhorn_tripletap, airhorn_default]

'''
GAMELOOP
'''

while True:


    if state == 0:
        if not jojo.is_playing():
            jojo.play()
        scr0 = 0 #scores para o player 1 e 2
        scr1 = 0
        meme.draw()
        janela.draw_text("Pressione 1 para singleplayer",0,0,40,(0, 0, 0),"Comic Sans")
        janela.draw_text("Pressione 2 para multiplayer",0,50,40,(0, 0, 0),"Comic Sans")
        janela.draw_text("Pressione ESC para sair",0,90,40,(0, 0, 0),"Comic Sans")
        if(key.key_pressed("1")):
            state = 1
            jojo.stop()
        if(key.key_pressed("2")):
            state = 2
            jojo.stop()
        if(key.key_pressed("escape")):
            janela.close()
        janela.update()



    elif state == 1:
        if not despacito.is_playing():
            despacito.play()
        if (scr0 == 5) or (scr1 == 5):
            state = 3
            despacito.stop()
        move(key,pad0,pad1,wh,psd,psu,janela)
        cpu_move(pad2, pad3, wh, psd, psu, janela, bolas, bx)
        if not bx:
            if(pad0.collided(bolas)):
                bx = True
                by = False
                speed += 50
            if(pad1.collided(bolas)):
                bx = True
                by = True
                speed += 50
        if bx:
            if(pad2.collided(bolas)):
                bx = False
                by = False
                speed += 50
            if(pad3.collided(bolas)):
                bx = False
                by = True
                speed+= 50
        if bolas.y <= 0:
            by = True
        if bolas.y+40 >= wh:
            by = False
        ball_movement(bolas,bx,by,speed,ww,wh,janela)
        if bolas.x+40 < 0:
            scr1 +=1
            speed = 100
            bolas.x = 400
            bolas.y = 200
            mlg_boolean += 100
            mlg_text = random.choice(mlg)
            mlg_x = random.randint(0,ww)
            mlg_y = random.randint(0,wh)
            #despacito.increase_volume(10)
            (random.choice(horn_list)).play()
        if bolas.x > ww:
            scr0 +=1
            speed = 100
            bolas.x = 400
            bolas.y = 200
            mlg_boolean += 100
            mlg_text = random.choice(mlg)
            mlg_x = random.randint(0,ww)
            mlg_y = random.randint(0,wh)
            #despacito.increase_volume(10)
            (random.choice(horn_list)).play()
        render (bg,pad0,pad1,pad2,pad3,bolas,janela,scr0,scr1,mlg_boolean,mlg_text,mlg_x,mlg_y) #pra evitar um monte de .draw, criei uma função render
        if mlg_boolean > 0:
            mlg_boolean -= 1

#teste

    elif state == 2:
        if not despacito.is_playing():
            despacito.play()
        if (scr0 == 5) or (scr1 == 5):
            state = 3
            despacito.stop()
        rata = mouse.get_position()
        mmouse(rata, pad2, pad3, wh)
        move(key,pad0,pad1,wh,psd,psu,janela)
        move2(key,pad2,pad3,wh,psd,psu,janela)
        if not bx:
            if(pad0.collided(bolas)):
                bx = True
                by = False
                speed += 50
            if(pad1.collided(bolas)):
                bx = True
                by = True
                speed += 50
        if bx:
            if(pad2.collided(bolas)):
                bx = False
                by = False
                speed += 50
            if(pad3.collided(bolas)):
                bx = False
                by = True
                speed+= 50
        if bolas.y <= 0:
            by = True
        if bolas.y+40 >= wh:
            by = False
        ball_movement(bolas,bx,by,speed,ww,wh,janela)
        if bolas.x+40 < 0:
            scr1 +=1
            speed = 100
            bolas.x = 400
            bolas.y = 200
            mlg_boolean += 100
            mlg_text = random.choice(mlg)
            mlg_x = random.randint(0,ww)
            mlg_y = random.randint(0,wh)
            (random.choice(horn_list)).play()
        if bolas.x > ww:
            scr0 +=1
            speed = 100
            bolas.x = 400
            bolas.y = 200
            mlg_boolean += 100
            mlg_text = random.choice(mlg)
            mlg_x = random.randint(0,ww)
            mlg_y = random.randint(0,wh)
            (random.choice(horn_list)).play()
        render (bg,pad0,pad1,pad2,pad3,bolas,janela,scr0,scr1,mlg_boolean,mlg_text,mlg_x,mlg_y) #pra evitar um monte de .draw, criei uma função render
        if mlg_boolean > 0:
            mlg_boolean -= 1

    elif state == 3:
        if not naruto.is_playing():
            naruto.play()
        if scr0 == 5:
            janela.draw_text("Player 1 é o vencedor",270,wh/2,40,(0, 0, 0),"Comic Sans")
        else:
            janela.draw_text("Player 2 é o vencedor",270,wh/2,40,(0, 0, 0),"Comic Sans")
        janela.draw_text("Enter: Jogar novamente",270,250,40,(0, 0, 0),"Comic Sans")
        janela.draw_text("Esc: Sair do jogo",290,300,40,(0, 0, 0),"Comic Sans")
        if(key.key_pressed("enter")):
            scr0 = 0
            scr1 = 0
            state = 0
            bolas.set_position(ww/2, wh/2)
            pad0.set_position(0, 163)
            pad1.set_position(0,200)
            pad2.set_position(750,163)
            pad3.set_position(750,200)
            naruto.stop()
            meme = GameImage(random.choice(fundo))
        elif(key.key_pressed("escape")):
            janela.close()
        janela.update()

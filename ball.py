def ball_movement(bolas,bx,by,speed,ww,wh,janela):

#    if bolas.y <= 0 or bolas.y >= wh:
#        by = not(by)

    if bx:
        bolas.move_x(speed * janela.delta_time())
    else:
        bolas.move_x(speed * -1 * janela.delta_time())
    if by:
        bolas.move_y(speed *  0.75 * janela.delta_time())
    else:
        bolas.move_y(speed * -0.75 * janela.delta_time())



def collision(pad0,pad1,pad2,pad3,bolas,bx,by,speed):
    if(pad0.collided(bolas)):
        bx = True
        by = False
        speed += speed
    if(pad1.collided(bolas)):
        bx = True
        by = True
        speed += speed
    if(pad2.collided(bolas)):
        bx = False
        by = False
        speed += speed
    if(pad3.collided(bolas)):
        bx = False
        by = True
        speed+=speed

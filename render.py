def render (bg,pad0,pad1,pad2,pad3,bolas,janela,scr0,scr1,mlg_boolean,mlg_text,mlg_x,mlg_y):
    bg.draw()
    pad0.draw()
    pad1.draw()
    pad2.draw()
    pad3.draw()
    bolas.draw()
    if mlg_boolean:
        janela.draw_text(mlg_text,mlg_x,mlg_y,30,(0,0,0), "Comic Sans")
    janela.draw_text(str(scr0),200,0,30,(0,0,0),"Comic Sans")
    janela.draw_text(str(scr1),600,0,30,(0,0,0),"Comic Sans")
    janela.update()
